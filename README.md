# About
An attempt to visualize all of the events in a `compute()` step for a Temporal Memory region.

Currently shows:

 - for a given set of input columns, the corresponding active and winning cells
 - two different segment views:
    - the active and winning cells + potential and connected synapses
    - the permanence value of every potential synapse, and whether it's connected or not

Internally it uses [Compose.jl](https://github.com/GiovineItalia/Compose.jl) to draw these visualizations.
