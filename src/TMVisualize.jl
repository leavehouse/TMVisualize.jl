module TMVisualize

using Colors, Compose, Measures

export RegionVis, draw_cells, draw_segments

const CellIndex = Tuple{Int, Int}
const Point{T} = Tuple{T, T} where {T}
const Permanence = Float64

# defining relations:
#
#    grid_W = N*2*r + (N-1)*sp_h
#    W = 2 * outer_pad + grid_W
#
# by fixing N, W, alpha = (r/sp_h), and outer_pad, r and sp_h can be determined
# In the case of fixed M > 1, can fix c = (sp_h/sp_v) to determine sp_v. The
# corresponding equations for height can be used:
#
#    grid_H = M*2*r + (M-1)*sp_v
#    H = 2 * outer_pad + S*grid_H + (S-1)*seg_sp_v
#
# where S is the number of segments and seg_sp_v is the spacing between segment
# views that are stacked vertically, one for each segment.
type RegionVis
    # The number of cells per column
    M::Int
    # The number of columns. Assuming N > 1
    N::Int
    # outer padding (like CSS margin)
    outer_pad::Measures.Length
    # total width
    W::Measures.Length
    # The (r/sp_h) ratio. 0 < alpha < +Inf
    alpha::Float64
    # Non-null only when M > 1. The ratio (sp_h / sp_v). 1 < c < +Inf
    c::Nullable{Float64}
    # radius
    r::Measures.Length
    # Horizontal spacing
    sp_h::Measures.Length
    # Non-null when M > 1. The vertical spacing.
    sp_v::Nullable{Measures.Length}
end

RegionVis(M::Int, N::Int, W::Measures.Length=18cm, alpha::Float64=0.9,
          c::Nullable{Float64}=Nullable(1.1), outer_pad::Measures.Length=2mm
         ) = begin
    if alpha < 0
        error("alpha cannot be negative, was found to be: $alpha")
    end
    if isnull(c) && M != 1
        error("c cannot be null unless M = 1. Currently M = $M")
    end
    if !isnull(c) && get(c) <= 1.
        error("c must be greater than 1, was found to be: $c")
    end

    grid_W = W - 2.*outer_pad
    r = grid_W / (2*N + (N - 1) / alpha)
    sp_h = r / alpha

    if M == 1
        sp_v = c = Nullable{Float64}()
    else
        sp_v = Nullable(r / (get(c) * alpha))
    end
    RegionVis(M, N, outer_pad, W, alpha, c, r, sp_h, sp_v)
end

function dims(rvis::RegionVis)
    (rvis.M, rvis.N)
end

function get_sp_v(rvis::RegionVis)
    isnull(rvis.sp_v) ? 0mm : get(rvis.sp_v)
end

function calc_grid_circle_centers(indices, r, sp_h, sp_v)::Dict{CellIndex, Point{Measures.Length}}
    Dict(begin
             x = r + (j-1)*(2*r + sp_h)
             y = r + (i-1)*(2*r + sp_v)
             (i,j) => (x, y)
         end
         for (i, j) in indices)
end

function create_grid_circles(indices, r::Measures.Length, sp_h, sp_v,
                             cell_stylist)
    centers = calc_grid_circle_centers(indices, r, sp_h, sp_v)
    (xs, ys) = ([], [])
    # TODO: is the collect() avoidable?
    sorted_cell_idxs = sort(collect(keys(centers)))
    for idx in sorted_cell_idxs
        c = centers[idx]
        push!(xs, c[1])
        push!(ys, c[2])
    end

    additional = cell_stylist(sorted_cell_idxs)

    on_cells = compose(context(),
        circle(reshape(xs, length(xs)), reshape(ys, length(ys)),
               [r]),
        linewidth([0.4]),
        additional...)
    (on_cells, centers)
end

# given `active`, `winning`, returns a cell stylist
function make_input_stylist(active, winning)
    function(cell_indices)
        light_gray = RGB(240/255, 240/255, 240/255)
        (fills, strokes) = ([], [])
        for idx in cell_indices
            push!(fills, idx in active ? "black" : light_gray)
            push!(strokes, idx in winning ? "orange" : nothing)
        end
        (stroke(strokes), fill(fills))
    end
end

# `pot_synapses` is a set of (cell, permanence) pairs, one for each
# potential synapse
# TODO: convert TM.CellID to tmvis.CellIndex automatically.
function make_perm_stylist(pot_synapses::Dict{CellIndex, Float64})
    function(cell_indices)
        fills = []
        for idx in cell_indices
            if haskey(pot_synapses, idx)
                perm = pot_synapses[idx]
                C = 40
                colors = colormap("Greens", C)
                perm_to_color(p) = begin
                                       bin = ceil(Int, C*p)
                                       bin == 0 ? colors[1] : colors[bin]
                                   end
                push!(fills, perm_to_color(perm))
            else
                push!(fills, "white")
            end
        end
        (fill(fills),)
    end
end


const SYN_BLUE = RGB(66/255, 161/255, 244/255)
const SYN_RED = RGB(232/255, 11/255, 11/255)

# `cell_stylist`, takes an iterator of cell indices and returns a tuple
# of strokes and/or fills and/or other Compose.jl properties that style
# the cells associated with the indices
function draw_segment(rvis::RegionVis, cell_stylist, pot_pool, conn_pool, syn_color)
    # TODO: these are duplicated from draw_segments(), find a way to avoid
    (r, sp_h, sp_v) = (rvis.r, rvis.sp_h, get_sp_v(rvis))

    (on_cells, centers) = create_grid_circles(MbyN(rvis), r, sp_h, sp_v,
                                              cell_stylist)

    if length(pot_pool) == 0
        on_cells
    else
        # defines the length of half of a side of a box
        padr = r + 0.25mm
        s = 2.*padr

        sorted_cell_idxs = sort(collect(keys(centers)))
        (xs, ys) = ([], [])
        fills = []
        for idx in sorted_cell_idxs
            if idx in pot_pool
                c = centers[idx]
                push!(xs, c[1] - padr)
                push!(ys, c[2] - padr)
                push!(fills, idx in conn_pool ? syn_color : nothing)
            end
        end

        syn_boxes = compose(context(),
                rectangle(xs, ys,
                          [s], [s]),
                fill(fills),
                stroke([syn_color]),
                linewidth([0.15]))

        compose(context(), on_cells, syn_boxes)
    end
end

# converts the incremental CellID in TemporalMemory.jl to (row, col) coords.
# The conversion is that the cells are imagined to be organized into a
# rectangular array. TM.CellID is obtained by incrementally labeling all the
# cells, which are stored column-major, so that cells in column 1 are given
# CellIDs 1, ..., M, and in general cells in column k are given CellIDs
# (k-1)*M + 1, ..., kM.
function to_rowcol(rvis::RegionVis, cid)
    M = rvis.M
    # The cells are s
    rem = cid % M

    if rem == 0
        (M, div(cid, M))
    else
        (rem, div(cid, M) + 1)
    end
end

function MbyN(rvis::RegionVis)
    Iterators.product(1:rvis.M, 1:rvis.N)
end

function draw_cells(rvis::RegionVis, cell_stylist)
    (r, sp_h, sp_v) = (rvis.r, rvis.sp_h, get_sp_v(rvis))
    (on_cells, _) = create_grid_circles(MbyN(rvis), r, sp_h, sp_v,
                                        cell_stylist)
    cells = compose(context(rvis.outer_pad, rvis.outer_pad, 1, 1), on_cells)
    grid_H = grid_height(rvis)
    cells |> SVG("hello_$(step)_region_cells.svg", rvis.W, 2*rvis.outer_pad + grid_H)
    cells
end

function grid_height(rvis::RegionVis)
    rvis.M * 2 * rvis.r + (rvis.M - 1)*get_sp_v(rvis)
end

function segments_graphic_height(rvis::RegionVis, S::Int, grid_H, seg_sp_v)
    2*rvis.outer_pad + S*grid_H + (S - 1)*seg_sp_v
end

function draw_segments(rvis::RegionVis, seg_datas, step, input_stylist)
    S = length(seg_datas)
    seg_inputs_graphics = []
    seg_perms_graphics = []
    for (segid, seg_data) in seg_datas
        syns = Dict{CellIndex, Permanence}(
                   to_rowcol(rvis, cid) => perm
                   for (cid, perm) in seg_data.synapses)

        potential = keys(syns)
        connected = (to_rowcol(rvis, c) for c in seg_data.connected)

        perm_stylist = make_perm_stylist(syns)

        inputs_graphic = draw_segment(rvis, input_stylist,
                                      potential, connected,
                                      SYN_BLUE)
        perms_graphic = draw_segment(rvis, perm_stylist,
                                     potential, connected,
                                     SYN_RED)

        push!(seg_inputs_graphics, inputs_graphic)
        push!(seg_perms_graphics, perms_graphic)
    end

    grid_H = grid_height(rvis)
    seg_sp_v = 0.4 * grid_H

    (shifted_seg_inputs, shifted_seg_perms) = ([], [])
    for i in 1:S
        cx = rvis.outer_pad
        cy = rvis.outer_pad + (i-1)*(grid_H + seg_sp_v)
        push!(shifted_seg_inputs, (context(cx, cy, 1, 1),
                                   seg_inputs_graphics[i]))
        push!(shifted_seg_perms, (context(cx, cy, 1, 1),
                                  seg_perms_graphics[i]))
    end

    main_inputs_graphic = compose(context(), shifted_seg_inputs...)
    main_perms_graphic = compose(context(), shifted_seg_perms...)

    graphic_w = rvis.W
    graphic_h = segments_graphic_height(rvis, S, grid_H, seg_sp_v)
    graphic_size = (graphic_w, graphic_h)
    main_inputs_graphic |> SVG("hello_$(step)_segs_inputs.svg", graphic_size...)
    main_perms_graphic |> SVG("hello_$(step)_segs_perms.svg", graphic_size...)

    (main_inputs_graphic, main_perms_graphic)
end

end # end of module
